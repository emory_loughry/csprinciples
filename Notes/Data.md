# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- abstraction: the process of removing elements of a code or program that aren't relevant or that distract from more important elements 
- analog data: data that is represented in a physical way
- bias: computer systems that systematically and unfairly discriminate against certain individuals or groups of individuals in favor of others
- binary number system: bianary code that is represented by either a 1 or a 0
- bit: a binary digit, the smallest increment of data on a computer
- byte: a unit of data that is eight binary digits long
- classifying data: the process of analyzing structured or unstructured data and organizing it into categories based on file type, contents, and other metadata
- cleaning data: the process of fixing incorrect, incomplete, duplicate or otherwise erroneous data in a data set
- digital data: the electronic representation of information in a format or language that machines can read and understand
- filtering data: the process of examining a dataset to exclude, rearrange, or apportion data according to certain criteria.
- information: data is a collection of facts, while information puts those facts into context
- lossless data compression: a compression technique that does not lose any data in the compression process
- lossy data compression: the data in a file is removed and not restored to its original form after decompression. this data loss is not normally noticable. 
- metadata: data that provide information about other data
- overflow error: when the data type used to store data was not large enough to hold the data
- patterns in data: recurring sequences of data over time that can be used to predict trends
- round-off or rounding error: the difference between an approximation of a number used in computation and its exact (correct) value
- scalability: the measure of a system's ability to increase or decrease in performance and cost in response to changes in application and system processing demands.
