# Computers Can Repeat Steps

## Repeating Steps
    *learning objectives:*
    - Use a for loop to repeat code.
    - Use range to create a list of numbers

## Repeating With Numbers
- for loops:
    - one type of loop or way to repeat a statement or set of statements in a program
    - uses a variable and makes the variable take on each of the values in a list one at a time
    - list
: holds values in an order. Ex. numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    - make sure indentation is correct

Example of a for loop that prints the sum of all the numbers between 1 and 10:
```
sum = 0  # Start out with nothing      
things_to_add = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for number in things_to_add:
    sum = sum + number
print(sum)`
```

## What is a List?
- a list holds things in order
    - syntax: is inclosed in [ and ] and the values are enclosed in commas. strings within lists are enclosed in quotes. 

## The Range Function
- use to loop over a sequence of numbers

- simple example: If the range function is called with a single positive integer, it will generate all the integer values from 0 to one less than the number it was passed and assign them one at a time to the loop variable. The below code prints numbers 0 - 3 ending at 3 and starting at 0. 

    Code for above:
    ```
    for number in range(4):
        print(number)
    ```

- if two numbers are inputed (input is where 4 is) then the first number will print all the way up to the number before the second input. 
    - ex. if the imputs are (1, 10) the numbers 1 - 9 will print

- if three numbers are imputed it means that the start and end value have be specified as well as how many numbers the loop needs to count by. For example, the code below would print 1, 3, 5, 7, and 9.
```
for i in range(1, 11, 2):
    print(i)
```
    - the code can also count down by switching the numbers around. For example, inputing 10, 1, -1 would output 10, 9, 8, 7, 6, 5, 4, 3, and 2, respectively

## There’s a Pattern Here! and Using the Accumulator Pattern
The 5 steps in the accumulator pattern:
   - Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
   - Get all the data to be processed.
   - Step through all the data using a for loop so that the variable takes on each value in the data.
   - Combine each piece of the data into the accumulator.
   - Do something with the result.

Here's an example of the code shown by finding the sum of all the numbers between 1 and 100:
```
    # Step 1: Initialize accumulator
    sum = 0  # Start out with nothing

    # Step 2: Get data
    numbers = range(1, 101)

    # Step 3: Loop through the data
    for number in numbers:
        # STEP 4: ACCUMULATE
        sum = sum + number

    # Step 5: Process result
    print(sum)
```
A more difficult problem: What is the sum of all the even numbers between 0 and 100. 
```
# Step 1: Initialize accumulator
sum = 0  # Start out with nothing

# Step 2: Get data
numbers = range(0, 101, 2)

# Step 3: Loop through the data
for number in numbers:
    # Step 4: Accumulate
    sum = sum + number

# Step 5: Process result
print(sum)
```

## Vocab and Additional Functions
*Accumulator Pattern*:
The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to sum a list of numbers.

*Body of a Loop*:
The body of a loop is a statement or set of statements to be repeated in a loop. Python uses indention to indicate the body of a loop.

*Indention*:
Indention means that the text on the line has spaces at the beginning of the line so that the text doesn’t start right at the left boundary of the line. In Python indention is used to specify which statements are in the same block. For example the body of a loop is indented 4 more spaces than the statement starting the loop.

*Iteration*:
Iteration is the ability to repeat a step or set of steps in a computer program. This is also called looping.

*List*:
A list holds a sequence of items in order. An example of a list in Python is [1, 2, 3].

*Loop*:
A loop tells the computer to repeat a statement or set of statements.

*def*:
The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.

*for*:
A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.

*print*:
The print statement in Python will print the value of the items passed to it.
