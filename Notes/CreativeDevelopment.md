## Big Idea 1: Creative Development
1. code segment: a portion of an object file that contains executable instructions
2. collaboration: a situation in which multiple parties work toward a common goal.
3. comments: text notes added to the program to provide explanatory information about the source code
4. debugging: the process of finding and fixing errors or bugs in the source code of any software
5. event-driven programming: a programming paradigm in which the flow of the program is determined by events such as user actions (mouse clicks, key presses), sensor outputs, or message passing from other programs or threads.
6. incremental development process: a software method in which the functionality of the system is divided into multiple, separate modules
7. iterative development process: a way of breaking down the software development of a large application into smaller chunks.
8. logic error: error that occurs when there is a fault in the logic or structure of the problem
9. overflow error: error that occurs when the data type used to store data was not large enough to hold the data
10. program: a specific set of ordered operations for a computer to perform
11. program behavior: how the program behaves
12. program input: data that is entered into or received by a computer 
13. program output: how the computer presents the results of the process
14. prototype: a rudimentary working model of a product or information system
15. requirements: a condition or capability needed by a user to solve a problem or achieve an objective
16. runtime error: an error that occurs while the program is running after being successfully compiled
17. syntax error: mistakes such as spelling and punctuation errors, incorrect labels, and so on, which cause an error message
18. testing: the process of evaluating a system or its component(s)
19. user interface: the way through which a user interacts with an application or a website.
