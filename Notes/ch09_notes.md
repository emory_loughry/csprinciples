## Using Repetition with Strings
**Learning Objectives:**
- Show how the accumulator pattern works for strings.
- Show how to reverse a string.
- Show how to mirror a string.
- Show how to use a while loop to modify a string.

**The Five Steps in the Accumulator Pattern:**
- Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
- Get all the data to be processed.
- Step through all the data using a for loop so that the variable takes on each value in the data.
- Combine each piece of the data into the accumulator.
- Do something with the result.

**Original Text:**
```
# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "Rubber baby buggy bumpers."

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = new_string + letter

# Step 5: Process result
print(new_string)
```
The final result is all the letters being copied from 'phrase' to 'new_string'

## Reversing Text

**Code #2:**
```
# Step 1: Initialize accumulators
new_string_a = ""
new_string_b = ""

# Step 2: Get data
phrase = "Happy Birthday!"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string_a = letter + new_string_a
    new_string_b = new_string_b + letter

# Step 5: Process result
print("Here's the result of using letter + new_string_a:")
print(new_string_a)
print("Here's the result of using new_string_b + letter:")
print(new_string_b)
```
- This code changes because the string is combined before rather than afterward, changing how values are accumulated.
- The new_string_a is in reverse because the latters are being added to the beginning instead of the end

## Mirroring Text
```
# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "This is a test"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = letter + new_string + letter

# Step 5: Process result
print(new_string)
```
- This new code mirrors the variable 'phrase'

## Modifying Text

