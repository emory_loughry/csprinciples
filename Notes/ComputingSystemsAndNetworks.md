# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth: The maximum amount of data transmitted over an internet connection in a given amount of time.
- Computing device: A functional unit that can perform substantial computations, including numerous arithmetic operations and logic operations without human intervention.
- Computing network: interconnected computing devices that can exchange data and share resources with each other
- Computing system: integrated devices that input, output, process, and store data and information
- Data stream: the process of transmitting a continuous flow of data (also known as streams) typically fed into stream processing software to derive valuable insights.
- Distributed computing system: making multiple computers work together to solve a common problem
- Fault-tolerant: the ability of a system (computer, network, cloud cluster, etc.) to continue operating without interruption when one or more of its components fail.
- Hypertext Transfer Protocol (HTTP): an application protocol for distributed, collaborative, hypermedia information systems that allows users to communicate data on the World Wide Web.
- Hypertext Transfer Protocol Secure (HTTPS): a combination of the Hypertext Transfer Protocol (HTTP) with the Secure Socket Layer (SSL)/Transport Layer Security (TLS) protocol.
- Internet Protocol (IP) address: a series of numbers that identifies any device on a network
- Packets: a small segment of a larger message
- Parallel computing system: the study, design, and implementation of algorithms in a way as to make use of multiple processors to solve a problem.
- Protocols:  a standardized set of rules for formatting and processing data
- Redundancy: a system design in which a component is duplicated so if it fails there will be a backup
- Router: a networking device that connects computing devices and networks to other networks
