# Intro To Networking Notes
## Chapter 1
### 1.1 Communicating at a distance
- historically people would communicate long distances by using wires and microphones. the problem with this is that there is no way for this method to be wide-spred. 
- telphones would first connect to an operator who would connect lines for people to speak into. This got more complicated as poeple wanted to speak over long distances
### 1.2 Computers Communicate Diffrently
- computers can send messages to each other (either short, medium, or long)
- Short messages: Check if a computer is online
- Medium messages: A picture or a long email
- Long messages: Movies or a peice of software
### 1.3 Early Wide Area Store-and-Forward Networks
- it was found that computers could send messages long distance if they were connected. this process took a long time so it was not widely spred/used
### 1.4 Packets and Routers
- in the 60's the idea of splitting messages into smallar parts; packets
- this minimized the amount of storage needed to send messages
- routers (used to be known as IMPs) were dedicated to sending messages to their specific location
- using IMPs data could be sent a Local Area Network to other computers hooked up to the same IMP
### 1.5 Adressing and Packets
- WHen a packet was sent it needed to know the IP of the source and the IP of the destination, sort of like a letter.
- IP (internet protocol): the "name" of the computer that the data was being sent to
- packets needed a set of instructions to know how to be put back together and decompressed
### 1.6 Putting It All Together
- Diffrent packets of the same message can be sent on diffrent paths but all end up in the same place
- packets can be sent on a offset so they can be ordered correctly regardless of send time
- The idea of the Internet comes from interworking which means all aspects  are working together
### 1.7 Glossary
- **address**: A number that is assigned to a computer so that messages can be routed to the computer
- **hop**: A single physical network connection. A packet on the Internet will typically make several “hops” to get from its source computer to its destination.
- **LAN**: Local Area Network. A network covering an area that is limited by the ability for an organization to run wires or the power of a radio transmitter
- **leased line**: An “always up” connection that an organization leased from a telephone company or other utility to send data across longer distances.
- **operator (telephone)**: A person who works for a telephone company and helps people make telephone calls.
- **packet**: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the Internet. The typical maximum packet size is between 1000 and 3000 characters.
- **router**: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the best outbound link to speed the packet to its destination.
- **store-and-forward network**: A network where data is sent from one computer to another with the message being stored for relatively long periods of time in an intermediate computer waiting for an outbound network connection to become available.
- **WAN**: Wide Area Network. A network that covers longer distances, up to sending data completely around the world. A WAN is generally constructed using communication links owned and managed by a number of different organizations.
### 1.8 Questions 
1. What did early telephone operators do?
    - b) Connected pairs of wires to allow people to talk
2. What is a leased line?
    - d) An “always on” telephone connection
3. How long might a message be stored in an intermediate com- puter for a store-and-forward network?
    - d) possibly as long as several hours  
4. What is a packet?
    - c) A portion of a larger message that is sent across a network
5. Which of these is most like a router?
    - a) A mail sorting facility
6. What was the name given to early network routers?
    - d) Interface Message Processors
7. In addition to breaking large messages into smaller seg- ments to be sent, what else was needed to properly route each message segment?
    - a) A source and destination address on each message segment 
8. Why is it virtually free to send messages around the world using the Internet?
    - c) Because so many people share all the resources
## Chapter 2
- four layers of the internet (The Four-Layer TCP/IP Model): the Link, Internetwork, Transport, and Application layer.
### 2.1 The Link Layer
- the link layer's job is to connect your computer to the local network as well as move data across the first hop/connection
- the link layer solves two issues:
    1. how to encode and send data across the link
    2. agreement on the frequinces of light to be used and how fast to send the data 
- wireless networking is the most common link layers. ex. wifi and celluar
- computers that work together have to "agree" on when to send data. packets make this sharing process easier
- computers use CSMA/CD (Carrier-sense multiple access with collision detection) to detect multiple computers sending data at the same time
### 2.2 The Interwork Layer (IP)
- when a packets final destination is the internet, its first link is a router. the sent packet has a source address and a destination address
- the first router looks at the destionation address and sends the packet that way. the routers along the way do their best to get the packet to it's destionation
- in the even of a delay, routers can send messages to each other
- routers can switch routes in the event of an outage 
### 2.3 The Transport Layer (TCP)
- in the event of packets being lost, badly delayed, or out of order, TCP solves the issue
- when packets are sent they have both the source computer's address and an offset. 
- offset: used to reconstruct the message in a packet
- in the event the destination computer finds that packets are missing, it asks the source computer to resend the data
- the sending computer stores a copy of the data being sent. It may not delete this copy until the destination computer reports it has recived the packet
- window size: The amount of data that the sending computer is allowed to send before waiting for an acknowledgement.
- If the window size is too small, the transmission is slow, but if it is too big, it can overload routers. The window size is balanced depeneding on the data speeds
### 2.4 The Application Layer
- made of two halves:
    1. the server: runs on destination computer, waits for incoming connections
    2. the client: runs on scource computer, firefox and chrome are examples of web client applications
- your clients are connected to uniform resourse locators (URLs)
- 1980s: the first networked applications allowed users to login to remote connections, transfer files, send mail, and do chats
1990s: the World Wide Web application was being developed. it allowed reading and editing networked hypertext docs with images. the web today is the most common application layer
- http and https are examples of defined application protocols. application protocols describe how the messages are sent in the two halves
### 2.5 Stacking the Layers
- when writing a network application, you only interact with the transport ayer
### 2.6 Glossary
- **client**: In a networked application, the client application is the one that requests services or initiates connections.
- **fiber optic**: A data transmission technology that encodes data using light and sends the light down a very long strand of thin glass or plastic. Fiber optic connections are fast and can cover very long distances.
- **offset**: The relative position of a packet within an overall message or stream of data.
- **server**: In a networked application, the server application is the one that responds to requests for services or waits for incoming connections. 
- **window size**: The amount of data that the sending computer is allowed to send before waiting for an acknowledgement.
### 2.7 Questions
1. Why do engineers use a “model” to organize their approach to solving a large and complex problem?
    - c) Because they can break a problem down into a set of smaller problems that can be solved independently
2. Which is the top layer of the network model used by TCP/IP networks?
    - a) Application
3. Which of the layers concerns itself with getting a packet of data across a single physical connection?
    - d) Link 
4. What does CSMA/CD stand for?
    - a) Carrier Sense Multiple Access with Collision Detection
5. What is the goal of the Internetwork layer?
    - b) Get a packet of data moved across multiple networks from its source to its destination
6. In addition to the data, source, and destination addresses, what else is needed to make sure that a message can be reassembled when it reaches its destination?
    - a) An offset of where the packet belongs relative to the beginning of the message
7. What is "window size"?
    - d) The maximum amount of data a computer can send before receiving an acknowledgement
8. In a typical networked client/server application, where does the client application run?
    - a) On your laptop, desktop, or mobile computer
9. What does URL stand for?
    - c) Uniform Resource Locator
## Chapter 3
- the link layer is the lowest layer, meaning it is closest to the physical network media
### 3.1 Sharing the Air
- devices connected to the Wifi and using the internet are sending and receiving data with a small, low-powered radio
- All WIFI radio's have their own serial numbers that are called the media access control (MAC address)
- usually shown in the form of 0f:2a:b3:1f:b3:1a
- MAC addresses are kind of like a "from" or "to" address on a postcard. Every packet sent across the WiFi has a source and destiation.
- the first router that handles your computer's packet is called a gateway or base station
### 3.2 Courtesy and Coordination
- computers have to coordinate how they send data due to the fact that many share the same radio frequencies
- first method is called the "Carrier Sense". It first listens for a transmission. Then, if there is already a transmission in progress, it will wait until the transmission finishes.
- if two computers try to transmit packets at the same time, they will corrupt and the packets will be lost
### 3.3 Coordination in Other Link Layers
- when a link layer has many transmitting stations that need to operate at 100% efficiency for a long time, it will have a token that indicates when each station is given the opportunity to transmit data
- when a token is received and a packet is ready to be sent, a station will send the packet. the station then waits for the token to come back. 
- The token is like a room of people passing around an object and only the person who has the object can speak (ex. a "talking pillow")
### 3.4
- A benefit of layered architecture is that engineers can focus on one layer at a time
### 3.5 Glossary
- **base station**: Another word for the first router that handles your
packets as they are forwarded to the Internet. 
- **broadcast**: Sending a packet in a way that all the stations connected to a local area network will receive the packet.
- **gateway**: A router that connects a local area network to a wider
area network such as the Internet. Computers that want to send
data outside the local network must send their packets to the
gateway for forwarding.
- **MAC Address**: An address that is assigned to a piece of network
hardware when the device is manufactured.
- **token**: A technique to allow many computers to share the same
physical media without collisions. Each computer must wait until
it has received the token before it can send data. 
### 3.6 Questions
1. When using a WiFi network to talk to the Internet, where
does your computer send its packets?
    - a) A gateway
2. How is the link/physical address for a network device assigned?
    - c) By the manufacturer of the link equipment
3. Which of these is a link address?
    - a) 0f:2a:b3:1f:b3:1a
4. How does your computer find the gateway on a WiFi network?
    - b) It broadcasts a request for the address of the gateway
5. When your computer wants to send data across WiFi, what
is the first thing it must do?
    - a) Listen to see if other computers are sending data
6. What does a WiFi-connected workstation do when it tries to
send data and senses a collision has happened?
    - d) Stop transmitting and wait a random amount of time before
restarting
7. When a station wants to send data across a “token”-style
network, what is the first thing it must do?
    - d) Wait until informed that it is your turn to transmit
## Chapter 4: Internetwork
- this layer is responsible for figuring out the route of the packet as well as sending it on its way
### 4.1: Internet Protocol Addresses
- IP adresses are used instead of the MAC address from the link layer becuase if we used the MAC address, it would require tracking every device in the world
- there are two versions of IP addresses
1. IPv4: old, consists of four numbers seperated by dots
    - example: 167.79.8.65
    - numbers can only be 0-255
2. IPv6
    - this one is longer and contains hexidecimal digits as well as numbers
    - can be broken into two parts
    1. the network number, is the first two numbers
        - similar to a zip code, multiple computers in a single internet connection can use
        - makes the system less complicated because computers appear just as their network number
    2. the host identifier, is the last two numbers in the address
        - singles out the individual computers
### 4.2 How Routers Determine the Routes
- routers have to learn the correct routes so packets can reach their destination
- they do this by talking to their neighbors. if they don't know, they ask theirs, and so on. at some point, the route is found and it is sent back to the orignal router, who then sends the packet on its way and stores the infor for later use
- mapping the route is called a routing table
### 4.3 When Things Get Worse and Better
- if the outbound links fails then the router discards all of the entries and has to rediscover the route
- the router is capable of asking its surrounding neighbors if they have any new or better route
### 4.4 Determining Your Route
- each place the packet stops only knows the next place, until the chain ends
- this means the internet does not know the exsact route your packet is going to take
- however, your computer can esitmate where a packet will travel with a traceroute command
- how time to live works:
    - when a packet is sent, it is designated a Time To Live. very time it passes through a router, it subtracts 1. When it hits zero, it gets trashed
    - the router then sends a message back saying the message was trashed. 
### 4.5 Getting an IP Address
- when a computer moves locations, it gets a new IP
- Dynamic Host Configuration Protocol: gives the computer the ability to mave from one network to another
### 4.6 A Diffrent Kind of Address Reuse
- if an address starts with a 192.168 means that it is non routable
- this helps routers conserve the real, routable addresses and use the non-routable ones for workstations that move networks
### 4.7 Global IP Address Allocation
- at the top of IP address allocations are 5 Regional Internet Registratrie (each one allocates for a major geographic area)
- more computers are being made with the IPv6 which is phasing out the IPv4
### 4.8 Summary
- the IP layer is not 100% percent reliable but is mostly accurate
- just pushes the packets and problems on to the transport layer which sorts out all of the problems
### 4.9 Glossary
- **core router**: A router that is forwarding traffic within the core of
the Internet.
- **DHCP**: Dynamic Host Configuration Protocol. DHCP is how a
portable computer gets an IP address when it is moved to a new
location
- **edge router**: A router which provides a connection between a
local network and the Internet. Equivalent to “gateway”.
- **Host Identifier**: The portion of an IP address that is used to
identify a computer within a local area network.
- **IP Address**: A globally assigned address that is assigned to a
computer so that it can communicate with other computers that
have IP addresses and are connected to the Internet. To simplify
routing in the core of the Internet IP addresses are broken into
Network Numbers and Host Identifiers. An example IP address
might be “212.78.1.25”.
- **NAT**: Network Address Translation. This technique allows a single
global IP address to be shared by many computers on a single
local area network.
- **Network Number**: The portion of an IP address that is used to
identify which local network the computer is connected to.
- **packet vortex**: An error situation where a packet gets into an
infinite loop because of errors in routing tables
- **RIR**: Regional Internet Registry. The five RIRs roughly correspond
to the continents of the world and allocate IP address for the major geographical areas of the world.
- **routing tables**: Information maintained by each router that
keeps track of which outbound link should be used for each
network number
- **Time To Live (TTL)**: A number that is stored in every packet
that is reduced by one as the packet passes through each router.
When the TTL reaches zero, the packet is discarded.
- **traceroute**: A command that is available on many Linux/UNIX
systems that attempts to map the path taken by a packet as it
moves from its source to its destination. May be called “tracert”
on Windows systems.
- **two-connected network**: A situation where there is at least two
possible paths between any pair of nodes in a network. A two-way connected network can lose any single link without losing overall
connectivity.
### 4.10 Questions
1. What is the goal of the Internetworking layer?
    ✓ Move packets across multiple hops from a source to destination computer
    Move packets across a single physical connection
    Deal with web server failover
    Deal with encryption of sensitive data
2. How many different physical links does a typical packet cross from its source to its destination on the Internet?
    1
    4
    ✓ 15
    255
3. Which of these is an IP address?
    0f:2a:b3:1f:b3:1a
    ✓ 192.168.3.14
    www.khanacademy.com
    @drchuck
4. Why is it necessary to move from IPv4 to IPv6?
    Because IPv6 has smaller routing tables
    Because IPv6 reduces the number of hops a packet must go across
    ✓ Because we are running out of IPv4 addresses
    Because IPv6 addresses are chosen by network hardware manufacturers
5. What is a network number?
    ✓ A group of IP addresses with the same prefix
    The GPS coordinates of a particular LAN
    The number of hops it takes for a packet to cross the network
    The overall delay packets experience crossing the network
6. How many computers can have addresses within network number "218.78"?
    650
    6500
    ✓ 65000
    650000
7. How do routers determine the path taken by a packet across the Internet?
    The routes are controlled by the IRG (Internet Routing Group)
    ✓ Each router looks at a packet and forwards it based on its best guess as to the correct outbound link
    Each router sends all packets on every outbound link (flooding algorithm)
    Each router holds on to a packet until a packet comes in from the destination computer
8. What is a routing table?
    A list of IP addresses mapped to link addresses
    A list of IP addresses mapped to GPS coordinates
    A list of network numbers mapped to GPS coordinates
    ✓ A list of network numbers mapped to outbound links from the router
9. How does a newly connected router fill its routing tables?
    By consulting the IANA (Internet Assigned Numbers Authority)
    By downloading the routing RFC (Request for Comments)
    By contacting the Internet Engineering Task Force (IETF)
    ✓ By asking neighboring routers how they route packets
10. What does a router do when a physical link goes down?
    ✓ Throws away all of the routing table entries for that link
    Consults the Internet Map (IMAP) service
    Does a Domain Name (DNS) looking for the IP address
    Sends all the packets for that link back to the source computer
11. Why is it good to have at least a "two-connected" network?
    Because routing tables are much smaller
    Because it removes the need for network numbers
    Because it supports more IPv4 addresses
    ✓ Because it continues to function even when a single link goes down
12. Do all packets from a message take the same route across the Internet?
    Yes
    ✓ No
13. How do routers discover new routes and improve their routing tables?
    Each day at midnight they download a new Internet map from IMAP
    ✓ They periodically ask neighboring routers for their network tables
    They randomly discard packets to trigger error-correction code within the Internet
    They are given transmission speed data by destination computers
14. What is the purpose of the "Time to Live" field in a packet?
    ✓ To make sure that packets do not end up in an "infinite loop"
    To track how many minutes it takes for a packet to get through the network
    To maintain a mapping between network numbers and GPS coordinates
    To tell the router the correct output link for a particular packet
15. How does the "traceroute" command work?
    ✓ It sends a series of packets with low TTL values so it can get a picture of where the packets get dropped
    It loads a network route from the Internet Map (IMAP)
    It contacts a Domain Name Server to get the route for a particular network number
    It asks routers to append route information to a packet as it is routed from source to destination
16. About how long does it take for a packet to cross the Pacific Ocean via an undersea fiber optic cable?
    0.0025 Seconds
    ✓ 0.025 Seconds
    0.250 Seconds
    2.5 Seconds
17. On a WiFi network, how does a computer get an Internetworking (IP) address?
    ✓ Using the DHCP protocol
    Using the DNS protocol
    Using the HTTP protocol
    Using the IMAP protocol
18. What is Network Address Translation (NAT)?
    It looks up the IP address associated with text names like "www.dr-chuck.com"
    It allows IPv6 traffic to go across IPv4 networks
    It looks up the best outbound link for a particular router and network number
    ✓ It reuses special network numbers like "192.168" across multiple network gateways at multiple locations
19. How are IP addresses and network numbers managed globally?
    ✓ There are five top-level registries that manage network numbers in five geographic areas
    IP addresses are assigned worldwide randomly in a lottery
    IP addresses are assigned by network equipment manufacturers
    IP addresses are based on GPS coordinates
20. How much larger are IPv6 addresses than IPv4 addresses?
    They are the same size
    IPv6 addresses are 50% larger than IPv4 addresses
    ✓ IPv6 addresses are twice as large as IPv4 addresses
    IPv6 addresses are 10 times larger than IPv4 addresses
21. What does it mean when your computer receives an IP address that starts with "169.."?
    Your connection to the Internet supports the Multicast protocol
    ✓ The gateway is mapping your local address to a global address using NAT
    There was no gateway available to forward your packets to the Internet
    The gateway for this network is a low-speed gateway with a small window size
22. If you were starting an Internet Service Provider in Poland, which Regional Internet Registry (RIR) would assign you a block of IP addresses.
    ARIN
    LACNIC
    ✓ RIPE NCC
    APNIC
    AFRNIC
    United Nations
### additional assignment
**10011110.111011.11100001.1110011**
## Chapter 5
- The Domain Name system: allows for websites to be accessed by their domain name instead of the ip addresses
- When a computer uses a website it looks up the IP adress that coresponds with the domain
### 5.1 Allocating Domain Names
- The DNS (Domain Name System) is a system that allows end users to use symbolic names for servers, opposed to using numeric IP addresses.
- Top of the domain name hierarchy is an organization called International Corporation for Assigned Network Names and Numbers (ICANN)
    - ICANN also assigns top-level domains like .com, .edu, .org, .club, and .help
- Once a domain name is assigned to an organization, the controlling organization is allowed to assign subdomains within the domain
- ex: Educause could give the university of Michigan a subdomain called umich, so the subdomain and domain together would be umich.edu
### 5.2 Reading Domain Names
- IP addresses are read from left to right in a way, with the prefix (the least specific part of the IP address) to the right, which is the most specific
- Domain names are read from right to left, the right being the least specific, and the left being the most specific
### 5.3 Summary
- Domain names can be bought
- The Domain Name System allows end users to use symbolic names for servers instead of numeric Internet Protocol addresses
### 5.4 Glossary
- DNS: Domain Name System. A system of protocols and servers
that allow networked applications to look up domain names and
retrieve the corresponding IP address for the domain name.
- domain name: A name that is assigned within a top-level domain. For example, khanacademy.org is a domain that is assigned
within the “.org” top-level domain.
- ICANN: International Corporation for Assigned Network Names
and Numbers. Assigns and manages the top-level domains for
the Internet.
- registrar: A company that can register, sell, and host domain
names.
- subdomain: A name that is created “below” a domain
name. For example, “umich.edu” is a domain name and
both “www.umich.edu” and “mail.umich.edu” are subdomains
within “umich.edu”.
- TLD: Top Level Domain. The rightmost portion of the domain
name. Example TLDs include “.com”, “.org”, and “.ru”. Recently,
new top-level domains like “.club” and “.help” were added.
### Questions
1. What does the Domain Name System accomplish?
    - It allows network-connected computers to use a textual name for a computer and look up its IP address
2. What organization assigns top-level domains like ".com", ".org", and ".club"?
    - ICANN - International Corporation for Assigned Network Names
3. Which of these is a domain address?
    - www.khanacademy.org
4. Which of these is *not* something a domain owner can do with their domain?
    - Create new top-level domains
## Chapter 6
- The transport layer does not attempt to guarantee the delivery of a packet
- handles reliability and message reconstruction
### 6.1 Packet Headers
- a link header is removed when it is recived on one link and a new header is added when it is sent to a new link
- TCP indicates where the data in each packet belongs 
### 6.2 Packet Reassembly and Retransmission
-  the destination computer looks at the offset to reassemble delivered files
- computers only send a certain amount of data before waiting for acknowledgement
- this prevents overwhelming the network
- this method is called window size
- by adjusting the window size it becomes easier to send data quickly
- the destination computer keeps track of the amount of time since it recived the last packet to make sure the computers are not waiting too long
### 6.3 The Transport Layer in Operation
- The sending computer must hold onto all data untill the destination computer has all of it
### 6.4 Application Clients and Servers
- the transport layer provides a reliable connection between two computers
- the application that initiates the connection is called the client
- the application that responds to the connection is called the server
### 6.5 Server Applications and Ports
- client application must know what server to connect to 
- ports are used to allow a client application to choose which server to connect to
- ex: 8080 means your browser is going to use web protocals to interact with the server and use port 8080 
### 6.6 Summary
- the transport layer is meant to make up for the two lower layers when they lose or reroute packets
- it reasembles or retransmits the data
## 6.7 Glossary
- **acknowledgement**: When the receiving computer sends a notification back to the source computer indicating that data has
been received.
- **buffering**: Temporarily holding on to data that has been sent or
received until the computer is sure the data is no longer needed.
- **listen**: When a server application is started and ready to accept
incoming connections from client applications.
- **port**: A way to allow many different server applications to be
waiting for incoming connections on a single computer. Each
application listens on a different port. Client applications make
connections to well-known port numbers to make sure they are
talking to the correct server application.
## 6.8 Questions
1. What is the primary problem the Transport (TCP) layer is supposed to solve?
    - c) Deal with lost and out-of-order packets
2. What is in the TCP header?
    - c) Port number and offset
3. Why is “window size” important for the proper functioning of
the network?
    - b) It prevents a fast computer from sending too much data on
a slow connection
4. What happens when a sending computer receives an acknowledgement from the receiving computer?
    - b) The sending computer sends more data up to the window
size
5. Which of these detects and takes action when packets are
lost?
    - d) Receiving computer
6. Which of these retains data packets so they can be retransmitted if a packets lost?
    - a) Sending computer
7. Which of these is most similar to a TCP port?
    - c) Apartment number
8. Which half of the client/server application must start first?
    - a) Client
9. What is the port number for the Domain Name System?
    - c) 53
10. What is the port number for the IMAP mail retrieval protocol?
    - d) 143
## Chapter 7
- the application layer is what defines the software used, like email, web, or networked video games
- the top most layer
### 7.1 Client and Server Applications
- a networked application must have a client and a server to function
- browsing a web address requires having the web application running on your computer
- the client is usually a web browser, and the server is a web server that provides resources such as web pages, images, and other files
### 7.2 Application Layer Protocols
- each pair of network applications has rules on how to govern the conversation
- the set of rules are called protocols
- https stands for Hypertext Transfer Protocol Secure
- https must be typed at the beggining of a url if you are using the https protocol
- https is common because it is fairly simple
### 7.3 Exploring the HTTP protocol
- The internet was created in 1985
- telnet application was built before the first TCP/IP network
- port 80 is where you find a webserver
- A status code of 200 means things went well
- A status code of 404 means that the document was not found
- A status code of 301 means the document has been moved
- 2xx= sucess, 4xx= client failure, 5xx= server failure
### 7.4 The IMAP protocol for Retrieving Mail
- IMAP is a more complex protocol that has to be secure.
- your mail is sent to a server while your computer is off
- the messages sent by the client and the server are not discriptive as they are not meant to be seen by the end user
### 7.5 Flow Control
- the transport layer waits for acknowledgement
- the ability to start and stop the sending application is called flow control
### 7.6 Writing Networked Applications
- the lower levels work together so well that making connections and sending data is relatively easy
### 7.7 Summary
- lower three layers makes it so the applications running on the application layer can focus on problems that need to be solved
- this allows for a large range of network applications
### 7.8 Glossary
- **HTML**: HyperText Markup Language. A textual format that
marks up text using tags surrounded by less-than and
greater-than characters. Example HTML looks like: <p>This
is <strong>nice</strong></p>.
- **HTTP**: HyperText Transport Protocol. An Application layer protocol that allows web browsers to retrieve web documents from web
servers.
- **IMAP**: Internet Message Access Protocol. A protocol that allows
mail clients to log into and retrieve mail from IMAP-enabled mail
servers.
- **flow control**: When a sending computer slows down to make
sure that it does not overwhelm either the network or the destination computer. Flow control also causes the sending computer
to increase the speed at which data is sent when it is sure that
the network and destination computer can handle the faster data
rates.
- **socket**: A software library available in many programming languages that makes creating a network connection and exchanging data nearly as easy as opening and reading a file on your
computer.
- **status code**: One aspect of the HTTP protocol that indicates
the overall success or failure of a request for a document. The
most well-known HTTP status code is “404”, which is how an HTTP
server tells an HTTP client (i.e., a browser) that it the requested
document could not be found.
- **telnet**: A simple client application that makes TCP connections
to various address/port combinations and allows typed data to be
sent across the connection. In the early days of the Internet, telnet was used to remotely log in to a computer across the network.
- **web browser**: A client application that you run on your computer
to retrieve and display web pages.
- **web server** : An application that deliver (serves up) Web pages
### 7.9 Questions
1. Which layer is right below the Application layer?
    - a) Transport
2. What kind of document is used to describe widely used Application layer protocols?
    - b) RFC
3. Which of these is an idea that was invented in the Application layer?
    - d) http://www.dr-chuck.com/
4. Which of the following is not something that the Application
layer worries about?
    - c) How the window size changes as data is sent across a socket
5. Which of these is an Application layer protocol?
    - a) HTTP
6. What port would typically be used to talk to a web server?
    - b) 80
7. What is the command that a web browser sends to a web
server to retrieve an web document?
    - d) GET
8. What is the purpose of the “Content-type:” header when you
retrieve a document over the web protocol?
    - a) Tells the browser how to display the retrieved document
9. What common UNIX command can be used to send simple
commands to a web server?
    - d) telnet
10. What does an HTTP status code of “404” mean?
    - d) Document not found
11. What characters are used to mark up HTML documents?
    - a) Less-than and greater-than signs < >
12. What is a common application protocol for retrieving mail?
    - d) IMAP
13. What application protocol does RFC15 describe?
    - a) telnet
14. What happens to a server application that is sending a large
file when the TCP layer has sent enough data to fill the window size and has not yet received an acknowledgement?
    - c) The application is paused until the remote computer
acknowledges that it has received some of the data
15. What is a “socket” on the Internet?
    - d) A two-way data connection between a pair of client and
server applications
16. What must an application know to make a socket connection
in software?
    - a) The address of the server and the port number on the server
## Chapter 8
- two ways to protect network activity
    1. protect the physical hardware
    2. encrypt the data
### 8.1 Encrypting and Dectrypting Data
- all encryptions have a key
- the ceaser cypher was used by the romans
### 8.2 Two Kinds of Secrets
- people used to call or talk to each other and tell each other the key to decrypt encryptions
- asymmetric key: a type of key where one key encrypts the data and one key decrypts the data, the concept was formed in the 1970's
### 8.3 Secure Sockets Layer
- scurity was added to the internet 20 years ago
- network engineers did not want to mess up the existing internet protocols so they added a partial layer between the transport and the application layers
### 8.4 Encrypting WEb Browser Traffic
- http or https to indicates whether the data is encrypted
- your browser will display a lock symbol by the domain name to show that it is encrypted
### 8.5 Certificates and Certificate Authorities
- rogue computers can send you public keys and use that to take your data because when you send back your encrypted data they now have the key to decrypt it
- the certificate authority will send you a public key to make sure you are on a trusted and secure site
### 8.6 Summary
- secure connections on the internet are called Secure Sockets Layer or the Transport Layer Security
### 8.7 Glossary
- **asymmetric key**: An approach to encryption where one (public)
key is used to encrypt data prior to transmission and a different
(private) key is used to decrypt data once it is received.
- **certificate authority**: An organization that digitally signs public
keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.
- **ciphertext**: A scrambled version of a message that cannot be
read without knowing the decryption key and technique.
- **decrypt**: The act of transforming a ciphertext message to a plain
text message using a secret or key.
- **encrypt**: The act of transforming a plain text message to a ciphertext message using a secret or key.
- **plain text**: A readable message that is about to be encrypted
before being sent.
- **private key**: The portion of a key pair that is used to decrypt
transmissions.
- **public key**: The portion of a key pair that is used to encrypt
transmissions.
- **shared secret**: An approach to encryption that uses the same
key for encryption and decryption.
- **SSL**: Secure Sockets Layer. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Transport Layer Security (TLS).
- **TLS**: Transport Layer Security. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer
(SSL).

### 8.8 Questions
1. How do we indicate that we want a secure connection when
using a web browser?
    - a) Use https:// in the URL
2. Why is a shared-secret approach not suitable for use on the
Internet?
    - b) It is difficult to distribute the secrets
3. What is the underlying mathematical concept that makes
public/private key encryption secure?
    - d) Prime numbers
4. Which of the keys can be sent across the Internet in plain
text without compromising security?
    - a) Encryption key
5. Where does the Secure Sockets Layer (SSL) fit in the fourlayer Internet architecture?
    - d) Between the Transport and Application layers
6. If you were properly using https in a browser over WiFi in a
cafe, which of the following is the greatest risk to your losing
credit card information when making an online purchase?
    - d) You have a virus on your computer that is capturing
keystrokes
7. With the Secure Sockets Layer, where are packets encrypted
and decrypted?
    - c) They are encrypted in your computer and decrypted in the
server
8. What changes to the IP layer were needed to make secure
socket layer (SSL) work?
    - a) No changes were needed
9. If a rogue element was able to monitor all packets going
through an undersea cable and you were using public/private key encryption properly, which of the following
would be the most difficult for them to obtain?
    - d) Which documents you retrieved from the servers
10. What is the purpose of a Certificate Authority in public/private key encryption?
    - c) To assure us that a public key comes from the organization
it claims to be from
11. The ARPANET network was in operation starting in the 1960s.
Secure Sockets Layer (SSL) was not invented util the 1980s.
How did the ARPANET insure the security of the data on its
network?
    - c) By making sure no one could access the physical links
12. Which of these answers is “Security is fun” encrypted with a
Caesar Cipher shift of 1.
    - c) Tfdvsjuz jt gvo
13. What Caesar Cipher shift was used to encrypt “V yvxr frphevgl”?
    - c) 13
