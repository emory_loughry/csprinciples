## What is Keras?
- a high level neural networks API (Application Programming Interface)
- written in python
- it relies on a specialized tensor munipulation library to handle low-level compution
- there are two models you can work with: sequential and functional
![Keres Models](KeresModels.png)

## Sequential Model
- deals with ordering or sequencing of layers within a model
- is a linear stack of layers
- can be created by passing a list of layer instances to the constructor 
![Sequential Model](SKeresModel.png)

## Functional Model
- used for defining complex models, such as multi-output models or models with shared layers
- three unique aspects:
    - defining the imput: you must create and define a standalone imput that specifies the shape of the imput data
    - connecting layers: layers are connected pairwise
    - creating the model: use the model class

## Our Project

