# Our Study Plan
1. **Understand the Exam Format and Content**: Start by becoming familiar with the exam's structure and subject matter. A performance task component plus a multiple-choice section make up the AP Computer Science Principles exam. The 74 questions in the multiple-choice part test your understanding of computer innovations, programming, data, and algorithms. You must finish two activities in the performance task section to show that you can apply computational thinking to solve problems in the real world. Make sure you are aware of the importance of each part as well as the time provided for it.
---
2. **Identify Your Weaknesses**: To choose the subjects you should concentrate on, take a diagnostic exam or look through the course outline. A wide range of subjects, including programming languages, data analysis, algorithms, computer networks, and cybersecurity, are covered in the AP Computer Science Fundamentals exam. Determine your areas of weakness and devote extra effort to studying those subjects.
---
3. **Review the Course Material**: Review the course material thoroughly, including the textbook, lecture notes, and assignments. Pay special attention to the topics you identified as your weaknesses. Make sure you understand the key concepts and can apply them to solve problems.
---
4. **Practice with Sample Questions**: To get a feel for the kinds of questions you might see on the exam, practice with sample questions. Sample tests can be found in study manuals or on the College Board website. Attempt to finish as many practice questions as you can, and be sure to check your responses to identify any errors.
---
5. **Practice the Performance Tasks**: In the performance task portion of the exam, you must use computational thinking to address real-world problems. Practice performing similar exercises to improve your problem-solving skills. Practice exams are available in study guides and online at the College Board. 
---
6. **Review and Evaluate**: Regularly assess your development and determine your strengths and limitations. In light of this, change your study strategy and pay more attention to your areas of weakness.
---
7. **Manage Your Time Effectively**: On the day of the exam, use time management skills successfully. To finish the exam on time, pace yourself and make sure you allot enough time for each section.

## Our Resources 
- [Khan Academy](https://www.khanacademy.org/computing/ap-computer-science-principles)
- [Practice Quizes](https://codehs.com/playlist/ap-cs-principles-exam-review-1780?)
- [Review the Test Itself](https://www.princetonreview.com/college-advice/ap-computer-science-principles-exam)

## Our Pointers

- Get at least 8 hours of sleep the night before.

- Don't be afraid to use stackoverflow.

- Don't stress yourself more than you already are, visualize that you will do well.
  
- Don't get burnt out while you're studying, take frequent breaks.

- **While test taking, try to follow these tips:**
>1. **Answer all questions in order**, don't skip around.
>2. **Carefully read each question** and make sure you understand it before answering.
>3. **try not to second guess yourself** unless you are absolutely sure your first answer is wrong.
>4. **Do not rely on the answers to questions being in a pattern**. They almost never will be.
>5. **Seek Help When Needed**: If you are struggling with a particular topic, don't hesitate to seek help from your teacher, tutor, or classmates. You can also find resources online, such as online courses, forums, and tutorials.
