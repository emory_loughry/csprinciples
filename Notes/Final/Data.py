import numpy as np
from random import randint
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle


trainSamples = []
trainLabels = []


# setting up training


# People who experienced side effects
for i in range(50):


randomYoung = randint(13, 64)
trainSamples.append(randomYoung)
trainLabels.append(1)


randomOld = randint(65, 100)
trainSamples.append(randomOld)
trainLabels.append(0)


# People who did not experience side effects
for i in range(1000):


randomYoung = randint(13, 64)
trainSamples.append(randomYoung)
trainLabels.append(0)


randomOld = randint(65, 100)
trainSamples.append(randomOld)
trainLabels.append(1)




# setting up Training Arrays


trainLabels = np.array(trainLabels)
trainSamples = np.array(trainSamples)
trainLabels, trainSamples = shuffle(trainLabels, trainSamples)


scaler = MinMaxScaler(feature_range=(0, 1))
scaledTrainSamples = scaler.fit_transform(trainSamples.reshape(-1,1))


#setting up tests


testSamples = []
testLabels = []


# People who experienced side effects
for i in range(50):


randomYoung = randint(13, 64)
testSamples.append(randomYoung)
testLabels.append(1)


randomOld = randint(65, 100)
testSamples.append(randomOld)
testLabels.append(0)


# People who did not experience side effects
for i in range(1000):


randomYoung = randint(13, 64)
testSamples.append(randomYoung)
testLabels.append(0)


randomOld = randint(65, 100)
testSamples.append(randomOld)
testLabels.append(1)




# setting up testing Arrays


testLabels = np.array(testLabels)
testSamples = np.array(testSamples)
testLabels, testSamples = shuffle(testLabels, testSamples)


scaledTestSamples = scaler.transform(testSamples.reshape(-1,1))



