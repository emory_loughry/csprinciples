import Data
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Activation, Dense
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy
from sklearn.metrics import confusion_matrix




model = Sequential([
Dense(units=16, activation="relu", input_shape = (1,)),
Dense(units=32, activation="relu"),
Dense(units=2, activation="softmax")
])


model.compile(
optimizer = Adam(learning_rate = 0.0001),
loss = 'sparse_categorical_crossentropy',
metrics = ['accuracy']
)


model.fit(
x = Data.scaledTrainSamples,
y = Data.trainLabels,
validation_split = 0.1,
batch_size = 10,
shuffle = True,
epochs = 60,
verbose = 2
)


predictions = model.predict(
x = Data.scaledTestSamples,
batch_size = 10,
verbose = 0
)


roundedPredictions = np.argmax(predictions, axis= -1)


cm = confusion_matrix(y_true = Data.testLabels, y_pred = roundedPredictions)
print(cm)

