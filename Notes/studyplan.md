## How is the APCSP exam scored?
The AP Computer Science Principles exam is scored 1-5, 5 being a perfect score. The multiple-choice section is worth 70% of your grade and is scored by machine. The performance task is worth 30% and scored by a College Board employee.
 
## How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4, or 3 on the exam?
This depends on your score on the Create Performance Task. I used [this calculater](https://www.albert.io/blog/ap-computer-science-principles-score-calculator/) to calculate these scores. 
- If you get a 6/6 on the CPT then you need a 32 - 50 to get a 3, a 51 - 59 to receive a 4, and a 52 - 70 to receive a 5
- If you get a 5/6 on the CPT then you need a 37 - 55 to get a 3, a 56 - 64 to receive a 4, and a 65 - 70 to receive a 5
- If you get a 4/6 on the CPT then you need a 42 - 60 to get a 3, a 61 - 69 to receive a 4, and a 70 to receive a 5
- If you get a 3/6 on the CPT then you need a 47 - 65 to get a 3, a 66 - 70 to receive a 4, and at this point you would be unable to receive a 5
- If you get a 2/6 on the CPT then you need a 52 - 70 to get a 3, and you would be unable to achieve a 4 or 5.
- If you get a 1/6 on the CPT then you need a 57 - 70 to get a 3, and you would be unable to achieve a 4 or 5.
- If you get a 0/6 on the CPT then you need a 62 - 70 to get a 3, and you would be unable to achieve a 4 or 5.   

## What is the Create Performance Task and how is it scored?
It is a program that students write while taking the course.
 Students need to submit their program code, a video running the program, and a written response to prompts about the program. How the Create Performance Task is scored can be found [here](https://apcentral.collegeboard.org/media/pdf/ap22-sg-computer-science-principles.pdf)

## On which of the 5 Big Ideas did I score best? On which do I need the most improvement?
I scored the best on **creative devlopment portion** of the test. I need the most work on the **impact of computing**. 

## What online resources are available to help me prepare for the exam in each of the Big Idea areas?
There are multiple links on the class website that can help you prepare for the test. On the AP website there is a more in depth discription of each of the Big Ideas for quick review.
