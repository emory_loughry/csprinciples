# Chapter 3: Names For Numbers

## Assigning a name 
### a variable is a name that holds a value
* the value of a variable is  able to be modified
* the value of a variable is called the assignment
### restrictions of variable naming
* must start with a letter or underscore
* can contain digits after first character
* cannot be a keyword. ex. and, def, elif, else, for, if, import, in, not, or, return, or while
* No spaces in name
## Expressions
* The right hand side of a variable can be an equation not just a simple value
* The value will then equal to the answer to the equation
### % (modulo)
* It returns the remainder to the variable
* ex. 4%2 returns 0 because there is no remainder
* ex. (taken straight from text) The result of x % y when x is smaller than y is always x. The value y can’t go into x at all, since x is smaller than y, so the result is just x.
### summary of expression types
* 1 + 2 -> Addition = 3
* 3 * 4 -> Multiplication = 12
* 1 / 3 -> Integer division = 0.333333333333 in Python 3
* 2.0 / 4.0 -> Division = 0.5
* 1 // 3 -> Floor division, the result is 0 in Python 3
* 2 % 3 -> Modulo = 2
## How Expressions are Evaluated
* equations are done in order of their symbols
* this means that math in code is done in a similar order to PEMDAS
* parantheses can be put around part of an expression to give that part priority

