## Vocab for Big Idea 5: Impact of Computing
1. asymmetric ciphers - a process that uses a pair of related keys (one public key and one private key) to encrypt and decrypt a message and protect it from unauthorized access or use.
2. authentication - verifying the identity of a user, process, or device, often as a prerequisite to allowing access to resources in an information system.
3. bias - computer systems that systematically and unfairly discriminate against certain individuals or groups of individuals in favor of others.
4. Certificate Authority (CA) - a trusted entity that issues Secure Sockets Layer (SSL) certificates. These digital certificates are data files used to cryptographically link an entity with a public key.
5. citizen science - the practice of public participation and collaboration in scientific research to increase scientific knowledge
6. Creative Commons licensing - they give everyone from individual creators to large institutions a standardized way to grant the public permission to use their creative work under copyright law.
7. crowdsourcing - the practice of obtaining information or input into a task or project by enlisting the services of a large number of people, either paid or unpaid, typically via the internet.
8. cybersecurity - the protection of internet-connected systems such as hardware, software and data from cyberthreats
9. data mining - the process of using computers and automation to search large sets of data for patterns and trends
10. decryption - a process that transforms encrypted information into its original format
11. digital divide - he gap between demographics and regions that have access to modern information and communications technology (ICT), and those that don't or have restricted access.
12. encryption - a way of translating data from plaintext (unencrypted) to ciphertext (encrypted)
13. intellectual property (IP) -  a computer code or program that is protected by law against copying, theft, or other use that is not permitted by the owner
14. keylogging - a type of surveillance technology used to monitor and record each keystroke on a specific computer.
15. malware - any program or file that is intentionally harmful to a computer, network or server.
16. multifactor authentication - a layered approach to securing data and applications where a system requires a user to present a combination of two or more credentials to verify a user's identity for login.
17. open access - free access to information and unrestricted use of electronic resources for everyone.
18. open source - software with source code that anyone can inspect, modify, and enhance.
19. free software - software that respects users' freedom and community
20. FOSS - free and open-source software
21. PII (personally identifiable information) - any representation of information that permits the identity of an individual to whom the information applies to be reasonably inferred by either direct or indirect means.
22. phishing - a scam where thieves attempt to steal personal or financial account information by sending deceptive electronic messages that trick unsuspecting consumers into disclosing personal information.
23. plagiarism - copying the content or ideas, or words of another source (without informing them) and expressing it as your own
24. public key encryption - a large numerical value that is used to encrypt data
25. rogue access point - a device not sanctioned by an administrator, but is operating on the network anyway. 
26. targeted marketing - lassifies potential customers, discovers their preferred content delivery mode and digital hangouts and then builds a marketing strategy aimed at that specific group.
27. virus - a type of malicious software, or malware, that spreads between computers and causes damage to data and software.
