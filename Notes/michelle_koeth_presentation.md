## Presentation Notes & Questions

# Edge Computing
- computing that acts on data at the source
- does all processing on board
- better daya collection
- less latency due to less network traffic
- redundancy and scalability
- esp32 and raspberry PI within edge computing

# TinyML
- low power comsumption
- "inferencing" standalone 

# ESP32 Hardware
- continuously coming out with new and more advanced models
- many variations

## The Challenge
- recognize "bully birds" from a bird feeder using Alot and TINYML

# The Data
- use TINYML to recognize the birds
- the data set used is a collection of 48,000 birds in America
- there are further criteria for male and female birds
- there are over 550 visual categories

## Questions 
- At what point would the ESP32 board not be able to handle the amount of deta?
- Why are some boards exspensive vs cheap?
- Throughout her career, has Ms. Koeth faced adversity due to gender discrimination?  
