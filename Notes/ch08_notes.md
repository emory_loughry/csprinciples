# Loops - For and While
**Learning Objectives:**
- Introduce the concept of a while loop, which will repeat the body of a loop while a logical expression is true.
- Introduce the concept of a logical expression, which is either true or false.
- Introduce the concept of an infinite loop, which is a loop that never ends.
- Compare and contrast while and for loops

## Example of a for loop

```
for counter in range(1, 10):
    print(counter)
```
- This for loop will print 1 - 9 on 9 different lines

## Introducing the While Loop
- Will repeat as long as the expression is true
    - Example of expression: x > 3
- Are commonly used when the amount of times the loop needs to run is unknown
    - Example: you play a game *until* there is a winner

The code below will loop as long as the number that you enter isn’t negative. It will add each non-negative number to the current sum. It will print out the sum and average of all of the numbers you have entered.
```
sum = 0
count = 0
message = "Enter a positive integer, or a negative number to stop"
value = input(message)

while int(value) > 0:
    print("You entered " + value)
    sum = sum + int(value)
    count = count + 1
    value = input(message)

print("The sum is: " + str(sum) +
      " the average is: " + str(sum / count))
```

## Infinite Loops
Example of a program that loops forever:
```
while 1 == 1:
    print("Looping")
    print("Forever")
```
- Due to the fact that 1 will always equal 1 the program will never stop running

## Counting with a While Loop
- Having a computer repeat a specific number of times can be done using a for loop and a list of numbers with the range function

- Can also also be done with a while loop:
```
counter = 1
while counter < 11:
    print(counter)
    counter = counter + 1 
```
- Counter variable is used to **increment** (increase the value by one). Above we continue the loop as long as the counter is less than the desired last value plus one

## Side by Side Comparison of a For Loop and a While Loop
| While Loops| For Loops      |
| ---------- | -------------- |
| First Line: creates the variable counter and sets its value to 1| First Line: creates the variable counter and the list of values from 1 to 10|
| Second Line: tests if the value of counter is less than 11, if true executes body of loop | Second Line: body of loop, prints the current value of counter and then changes counter to the next value in the list.    |
| Body of Loop: prints the current value of counter and then increments the value of counter | Body of Loop: Second Line |
| End of Loop: when counter is equal to 11 | End of Loop: for loop will stop when the range function gets to 10       |

While loop fixed:
```
counter = 1
while counter <= 10:
    print(counter)
    counter = counter + 1
```
- Risk with while loops is that they may become infinate loops due to forgetting to add the increments to your variable value. 

## Looping When We Don’t Know When We’ll Stop
- While loops are typically used for this

Process for finding the sqrt within 0.01:
1. Start by guessing 2.
2. Compute the guess squared.
3. Is the guess squared close to the target number? If it’s within 0.01, we’re done. We’ll take the absolute value of the difference, in case we overshoot. (In Python, abs is the absolute value function.)
4. If it’s not close enough, we divide the target number by our guess, then average that value with our guess.
5. That’s our new guess. Square it, and go back to Step 3.

Code that follows this process:
```
target = 6
guess = 2
guess_squared = guess * guess
while abs(target - guess_squared) > 0.01:
    closer = target / guess
    guess = (guess + closer) / 2.0
    guess_squared = guess * guess
print("Square root of", target, "is", guess)
```

## Nested For Loops
- body of a loop can include another loop!
Example of a program that generates all the times tables from 0 to 10:
```
for x in range(0, 11):
    for y in range(0, 11):
        print(str(x) + " * " + str(y) + " = " + str(x*y))
```

### How Many Times Does the Inner Loop Execute?
```
for x in range(0, 2):
    for y in range(0, 4):
        print('*')
```
By using the loops above, we can find the amount of stars that are going to be printed. 8 stars will be printed because 2 * 4 = 8. You just have to mulitply the about of times the outer loop executes and how many times the inner loop executes. 

## Summary
### Concepts
- Body of a Loop - The body of a loop is a statement or set of statements to be repeated in a loop. The body of a loop is indicated in Python with indention.
- Counter - A counter is a variable that is used to count something in a program.
- Increment - Increment means to increase the value of a variable by 1.
- Infinite Loop - An infinite loop is one that never ends.
- Logical Expression - An logical expression is either true or false. An example of a logical expression is x < 3.
