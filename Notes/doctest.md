# Doctest Notes

```
if __name__ == "__main__":
    import doctest
    doctest.testmod()
```

an example

```
def double(stuff):
    ```
    double(5)
    10
    ```
if __name__ == "__main__":
    import doctest
    doctest.testmod()
```

- the line double(5) is what you are testing. The line below is what you should get or what the code is testing for

