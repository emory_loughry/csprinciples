from turtle import *
import random
import time

screen = Screen()

line_turt = Turtle()
line_turt.penup()
line_turt.goto(600, -400)
line_turt.color("red")
line_turt.left(90)
line_turt.pendown()
line_turt.forward(800)

def turtle_place(color, num, turtles):
        turtle = Turtle(shape='turtle')
        turtle.penup()
        turtle.fillcolor(color)
        turtle.goto(-200, -400 + 200 * num)
        turtles.append(turtle)


colors = [
    'red', 'orange', 'blue', 'gold', 'yellow', 'dark green', 'cyan',
    'purple', 'maroon', 'pink', 'deep pink'
]


if __name__ == '__main__':
    turtles = []
    num_turtles = int(input("How many turtles? (1-5):  "))
    for num in range(num_turtles):
        colorchoice = input(f"What color?: ")
        turtle_place(colorchoice, num, turtles)

time.sleep(3)
speeds = [10, 20, 30, 40, 50]
rand_speed = random.choice(speeds)

racing = True

while racing:
    for turtle in turtles:
        turtle.forward(rand_speed)
        if turtle.xcor() >= 600:
            turtle.write("Winner!", move=False,align="center",font=("Georgia",30,"normal"))
            racing = False
            break

screen.exitonclick()

input()
