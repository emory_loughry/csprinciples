from random import randint

correct = 0

for q in range(10):
    num1 = randint(1,10)
    num2 = randint(1, 10)
    question = f"What is {num1} time {num2}?"
    answer = num1*num2
    response = int(input(question))
    if answer == response:
        print("thats right, well done")
        correct += 1
    else:
        print(f"No, im afraid the answer is {answer}.")
if correct >= 5:
    print(f"I asked you 10 questions and you got {correct} questions right")
else:
    print(f"I asked you 10 questions and you only got {correct} questions right. You have some work to do!")
