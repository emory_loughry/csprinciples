# Step 1: Initialize accumulator
product = 1  # Initialize to multiplication identity
# Step 2: Get data
number = 10
# Step 3: Loop through the data
while number < 21:
    # Step 4: Accumulate
    product = product * number
    number = number + 2
# Step 5: Process result
print(product)

# code has been corrected
