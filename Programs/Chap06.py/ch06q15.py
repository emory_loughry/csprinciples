def rectangle(turtle, width, height):
        turtle.forward(width)
        turtle.right(90)
        turtle.forward(height)
        turtle.right(90)
        turtle.forward(width)
        turtle.right(90)
        turtle.forward(height)
        turtle.right(90)

from turtle import *    
space = Screen()        
shelly = Turtle()       
rectangle(shelly, 50, 100)
#code has been written and draws a rectangle
