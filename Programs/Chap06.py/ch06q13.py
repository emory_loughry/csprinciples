def num_of_miles(tank_capacity, the_amount_left, miles_per_gallon):
    num_gallons = tank_capacity * the_amount_left
    num_miles = num_gallons * miles_per_gallon
    return num_miles

print(num_of_miles(10, 0.25, 32))
#function has been made
