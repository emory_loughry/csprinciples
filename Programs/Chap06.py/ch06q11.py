def cost_of_trip(miles, miles_per_gallon):
    num_gallons = miles / miles_per_gallon
    price_per_gallon = 3.45
    total = num_gallons * price_per_gallon
    return total

print(cost_of_trip(500, 26))
#function has been created
