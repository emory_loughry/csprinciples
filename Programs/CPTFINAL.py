import tkinter as tk

turnNum = 0

def isGameOver():
  
  over = 0
  xWon = 0
  
  if Buttons[0].cget('text') + Buttons[1].cget('text') + Buttons[2].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[0].cget('text') + Buttons[1].cget('text') + Buttons[2].cget('text') == "ooo":
    over += 1
    xWon += 0
  if Buttons[3].cget('text') + Buttons[4].cget('text') + Buttons[5].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[3].cget('text') + Buttons[4].cget('text') + Buttons[5].cget('text') == "ooo":
    over += 1
    xWon += 0
  if Buttons[6].cget('text') + Buttons[7].cget('text') + Buttons[8].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[6].cget('text') + Buttons[7].cget('text') + Buttons[8].cget('text') == "ooo":
    over += 1
    xWon += 0

  if Buttons[0].cget('text') + Buttons[3].cget('text') + Buttons[6].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[0].cget('text') + Buttons[3].cget('text') + Buttons[6].cget('text') == "ooo":
    over += 1
    xWon += 0
  if Buttons[1].cget('text') + Buttons[4].cget('text') + Buttons[7].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[1].cget('text') + Buttons[4].cget('text') + Buttons[7].cget('text') == "ooo":
    over += 1
    xWon += 0
  if Buttons[2].cget('text') + Buttons[5].cget('text') + Buttons[8].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[2].cget('text') + Buttons[5].cget('text') + Buttons[8].cget('text') == "ooo":
    over += 1
    xWon += 0

  if Buttons[0].cget('text') + Buttons[4].cget('text') + Buttons[8].cget('text') == "xxx":
    over += 1
    xWon += 1
  if Buttons[2].cget('text') + Buttons[4].cget('text') + Buttons[7].cget('text') == "ooo":
    over += 1
    xWon += 0

  text = ""
    
  if over == 1:
    
    if xWon == 1:
      text = "X"
    else:
      text ="O"
      
    endScreen = tk.Tk()
    
    endLable = tk.Label(endScreen, text = f"GAME OVER, {text} WON")
    endLable.pack()


def btnPressed(num):
  turn = ""

  global turnNum

  if turnNum % 2 == 0:
    turn = "x"
  else:
    turn = "o"

  turnNum += 1

  if Buttons[num].cget("text") != "x" and Buttons[num].cget("text") != "o" :
    Buttons[num].config(text = turn)
  isGameOver()
    
    
master = tk.Tk()

Buttons = [None] * 9

for i in range(9):
  Buttons[i] = tk.Button(master, text = "", command = lambda c=i: btnPressed(c))
  
buttQuit = tk.Button(master, text="Die", command=master.destroy)
  
Buttons[0].grid(row = 0, column = 0)
Buttons[1].grid(row = 0, column = 1)
Buttons[2].grid(row = 0, column = 2)

Buttons[3].grid(row = 1, column = 0)
Buttons[4].grid(row = 1, column = 1)
Buttons[5].grid(row = 1, column = 2)

Buttons[6].grid(row = 2, column = 0)
Buttons[7].grid(row = 2, column = 1)
Buttons[8].grid(row = 2, column = 2)

buttQuit.grid(row = 3, column = 1)
