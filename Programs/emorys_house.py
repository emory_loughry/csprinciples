import time
from turtle import *       

space = Screen()            
shelly  = Turtle()             
shelly.forward(100)           
shelly.left(90)              
shelly.forward(100)         
shelly.left(90)    
shelly.forward(100)
shelly.left(90)               
shelly.forward(100)     #Square setup

shelly.left(180)        
shelly.forward(100)
shelly.right(30)
shelly.forward(100)
shelly.right(120)
shelly.forward(100)     #Position and drawing for roof

shelly.right(120)
shelly.forward(100)     #Position for chimney
shelly.right(120)
shelly.forward(30)

shelly.color("red")
shelly.left(30)
shelly.forward(30)
shelly.right(90)
shelly.forward(15)
shelly.right(90)
shelly.forward(30)      #Chimney completed

shelly.penup()
shelly.forward(125)
shelly.left(90)
shelly.forward(10)
shelly.color("blue")
shelly.pendown()
shelly.forward(25)
shelly.left(90)
shelly.forward(45)
shelly.left(90)
shelly.forward(25)
shelly.left(90)
shelly.forward(45)      #door completed

shelly.left(180)
shelly.forward(45)
shelly.penup()
shelly.forward(30)
shelly.pendown()
r = -12
shelly.circle(r)
shelly.right(90)
shelly.forward(24)
shelly.left(90)
shelly.penup()
shelly.forward(12)
shelly.left(90)
shelly.forward(12)
shelly.pendown()
shelly.left(90)
shelly.forward(24)      #circular window completed!

time.sleep(5)
