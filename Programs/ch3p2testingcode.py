

#Driving from Chicago to Dallas code
distance = 924.7
mpg = 35.5
gallons = distance / mpg
cost_per_gallon = 3.65
cost_trip = gallons * cost_per_gallon
print("Cost to get from Chicago to Dallas")
print(cost_trip)
#
#Driving from Chicago to Dallas code but with my edits
distance = 924.7
mpg = 35.5
gallons = distance / mpg
cost_per_gallon = 3.45
cost_trip = gallons * cost_per_gallon
print("Cost to get from Chicago to Dallas")
print(cost_trip)
print("cost_per_gallon")
##This would print cost_per_gallon because of the quotes around the variable name. It is just considered a string within quotes
#
#Following the Ketchup Ooze code
dripMPH = .028
FPM = 5280.0
dripFPH = dripMPH * FPM
MPH = 60
dripFPM = dripFPH / MPH
print("Ketchup speed in feet per minute:")
print(dripFPM)
print("Ketchup speed to move 4 feet in minutes:")
print(4 / dripFPM)
#
#Walking through Assignment more Generally
a = 1
b = 12.3
c = "Fred"
d = b
##d is equal to b which is 12.3
#
var1 = 45
var1 = 17.3
var2 = var1
##var2 and var2 end up both being equal to 17.3
#
days_in_week = 7
print(days_in_week)
num_days = 7 * 3
print(num_days)
num_days2 = days_in_week * 3
print(num_days2)
##this program will print 7, 21, and 21
#
num_people = 10
appet = 2.00
entree = 9.89
dessert = 7.99
total_bill = (appet + entree) * 10 + dessert
print(total_bill)
##this program was written by me and will print 126.89
#
#Figuring out an Invoice
quantity1 = 2
unit_price1 = 7.56
total1 = quantity1 * unit_price1
quantity2 = 4
unit_price2 = 4.71
total2 = quantity2 * unit_price2
invoice_total = total1 + total2
print(invoice_total)
##this has 7 variables and will print 33.96
##you can also combine variables to limit confusion
##make sure your variable names make sense as well
#
apples = 4
pears = 3
total_cost = str((apples * 0.40) + (pears * 0.65))
print("total cost = " + total_cost)
##prints total cost = 3.55
##the 3rd and 4th line were edited
#
money = 4.00
pc_cost = 0.05
total_pc = 4.00/0.05
print(total_pc)
##written by me, should print 80.0
##how many paperclips can you buy?
#
#Chapter 3 summary and definitions
#Arithmetic Expression - An arithmetic expression contains a mathematical operator like - for subtraction or * for multiplication.
#Assignment - Assignment means setting a variable’s value. For example, x = 5 assigns the value of 5 to a variable called x.
#Assignment Dyslexia - Assignment Dyslexia is putting the variable name on the right and the value on the left as in 5 = x. This is not a legal statement.
#Integer Division - Integer division is when you divide one integer by another. In some languages this will only give you an integer result, but in Python 3 it returns a decimal value, unless you use the floor division operator, //.
#Modulo - The modulo, or remainder operator, %, returns the remainder after you divide one value by another. For example the remainder of 3 % 2 is 1 since 2 goes into 3 one time with a remainder of 1.
#Tracing - Tracing a program means keeping track of the variables in the program and how their values change as the statements are executed. We used a Code Lens tool to do this in this chapter.
#Variable - A variable is a name associated with computer memory that can hold a value and that value can change or vary. One example of a variable is the score in a game.
#
##Chapter 3 excercises
#2
print(16 % 7 == 2)
print((7 / 2) * 10 == 35)
print(2 / 4 == 0.5)
print(5 - 2 * 3 == -1)
print(3 * 2 + 1 == 7)
##all of the operaters have been filled in and all equations are True
#
#3
x = 6 * (1 - 2)
print(x)
##paranthisis have been adding and the equation now equals -6
#
#4
x = 12 * (2 - 3) + 4 * 2
print(x)
##paranthisis have been adding and the equation now equals -4
#
#5
miles = 500
miles_per_gallon = 26
num_gallons = miles / miles_per_gallon
price_per_gallon = 3.45
total = num_gallons * price_per_gallon
print(total)
##lines 3 and 5 have been edited and will print 66.3461538462
#
#6
today = 1
number_of_days = 82
that_day_number = today + number_of_days
that_day = that_day_number % 7
print(that_day)
##line 4 has been edited and now prints 6
#
#7
funds = 25
miles_per_gallon = 40
price_per_gallon = 3.65
num_gallons = funds / price_per_gallon
num_miles = num_gallons * miles_per_gallon
print(num_miles)
##lines 4 and 5 have been edited and will print 273.972602739726
#
#8
a_number = 12
b_number = 3
c_number = a_number * b_number
print(c_number)
##the fixed code
#
#9
price = 68
amount_off = 0.4
sale_reduction = price * amount_off
sale_price = price - sale_reduction
amount_off = 0.2
coupon_reduction = sale_price * amount_off
coupon_price = sale_price - coupon_reduction
print(coupon_price)
##lines 3 and 7 have been edited and will print 32.64
#
#10
a = 7
b = 2
c = a % b
print (c)
##syntax and semantic errors have been fixed
#
#11
num_people = 5
amount_per_person = 4
price = 0.5
total = amount_per_person / price
num_wings = total * num_people
print(num_wings)
##lines 4 and 5 have been edited and will print 40.00
#
#12
current_time = 10
new_time = 10 + 123
clock_time = new_time % 12
print(clock_time)
##line 3 has been edited and will now print 1
#
#13
total_minutes = 270
num_minutes = total_minutes % 60
num_hours = total_minutes / 60
print(num_hours)
print(num_minutes)
##lines 2 and 3 has been edited and will now print 4 and 30
#
#14
sub_total = 73
tax = 0.07
total = (sub_total * tax) + sub_total
print(total)
##line 3 has been edited and will now print 78.11
#
#15
pay_per_hour = 8
amount = 100
num_hours = amount / pay_per_hour
print(num_hours)
##syntax errors have been fixed and it now prints 12.5
#
#16
total_days = 1.30
num_hours = total_days * 24
num_minutes = num_hours * 60
num_seconds = num_minutes * 60
print(num_minutes)
print(num_seconds)
##code now prints 1872.0 and 112320.0
#
#17
price_per_apple = 0.6
num_pears = 3
price_per_pear = 1.2
funds = 8
funds_after_pears = 8 - (3 * 1.2)
num_apples = int(funds_after_pears / price_per_apple)
print(num_apples)
##code has been corrected and now prints 7
#
#18
gas_rate = 23
amount_gas = 15
distance = 112
gas_consumed = 112 / 23
gas_remaining = 15 - gas_consumed
print(gas_remaining)
##code should now print 10.1304347826087
#
#19
gallons_left = 10 / 4
miles_per_gallon = 32
print(gallons_left * miles_per_gallon)
##code prints 80
#
#20
m_s = 25
how_many_secs = 111 / m_s
print(how_many_secs)
##bullet speed question, prints 4.44
