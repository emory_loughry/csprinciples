product = 1      # Start with the multiplicative identity
numbers = [1, 2, 3, 4, 5]
for n in numbers:
    product = product * n
    
print(product)              # Print the result

#values have been entered and code now prints the product of 1 - 5
