def list_product(num_list):
    product = 1      
    for n in num_list:
        product = product * n
    return product
    
my_list = [1, 2, 3, 4, 5]
print(list_product(my_list))

# function has been added
