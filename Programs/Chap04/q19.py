#19
cash_per_week = 20
goal = 200
months_till = goal / cash_per_week
form_months_till = months_till / 4
print(f'It will take {form_months_till} months to earn {goal} if you make {cash_per_week} dollars a week.')
