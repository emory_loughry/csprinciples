#13
total_minutes = 270
num_minutes = total_minutes % 60
num_hours = (total_minutes - num_minutes) / 60
print(f"{total_minutes} is {num_hours} and {num_minutes} minutes.")
##the print command has now been edited and prints "270 is 4.0 and 30 minutes."
