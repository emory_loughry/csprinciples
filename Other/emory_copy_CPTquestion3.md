# Written Response (Question 3) for Create Performance Task


## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
```
The player controls a dot using keys. The player is trying to escape the    robots and avoid touching them. The robots can be destroyed by hitting each other. The goal of the game is to have the robots run into each other,      ending the game.
``` 
2. Describes what functionality of the program is demonstrated in the
   video.
```
The video demonstrates the player's dot moving around the screen and the    robots following the dot. The video also shows the robots hitting each other   and then 'dying'.
```
3. Describes the input and output of the program demonstrated in the
   video.
```
The input is the keys that the player touches to move the dot. The output is the player moving as well as the robots responding to the movement and      coming closer to the player.
```
## 3b. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
![list](List.png)
```
An empty list is created to hold 'robots'
```
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
![list](List_p2.png)
```
Line 37 shows the function place_robots relying on the length of the robots list to randomize the placement of the robots.
```
## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
```
The name of the list is robots
```
2. Describes what the data contained in the list represent in your
   program
```
The data contained in the list, at least when it is created, holds nothing. However, it is made to store the location of every robot. 
```
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
```
The list eliminates the need to have individual variables. Using the list also allows for the ability to add more robots. 
```
## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration

![3c](3c_image_1.png)
```
The name of this program is robot_crashed(the_bot). This program's parameter is the_bot. The point of this program is to determin if the robot has crashed. Sequencing and sselection is shown through the if statements and iteration is shown through the loop. 
```
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
![3c](3c_image_2.png)
```
robot_crashed(the_bot) is being called in check_collisions().
```
## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
```
robot_crashed(the_bot) checks if a robot has made contact with another robot, returning true is it has and false if it has not. The player wins the game by making the robots crash into each other. 
```
4. Explains in detailed steps how the algorithm implemented in the identified procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
```
The procedure takes the parameter the_bot and then compares it to a_bot every time the loop iterates through. The loop compares the x's and y's of the two variables, trying to find the same 'coordinates'. This is testing for a collision. If the two variables are not the same (the robots do not hit each other), the procedure will return false. If the variables are the same, then the robots have collided and the procedure will return true. 
```
## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.

> First call:
![3d](3d_image_1.png)
```
This call checks to see if the player will be in the same place as a robot when the game starts. This call is in safely_place_player().
```
> Second call:
![3d](3c_image_2.png)
```
This call is in check_collisions() and it checks if a collision was between the player and a robot or between two robots. 
```
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
```
The conditions are the placement of the robots and the intended placement of the player. 
```
> Condition(s) tested by the second call:
```
The conditions are the placement of the robots and if their 'coordinates' are the same.
```
3. Identifies the result of each call
>
> Result of the first call:
```
If the player would be placed on a robot, the player would recive new coordinates. If not, the player would be placed and the game would begin. 
```
> Result of the second call:
```
If the robots collide then one of the robots is turned to junk and the other is removed. 
```
