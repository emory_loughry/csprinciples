## Scoring
- You can get up to 6 points on the whole project. 1 point on each criteria. 

## Row 1 - Program Purpose and Function
| The Video | The Written Response |
| ---------- | -------------- |
| input | describes the overall purpose of the program |
| program functionality | describes what functionality of the program is demonstrated in the video. |
| output | describes the input and output of the program demonstrated in the video |

You will not get a point if:
- program is not run in video
- The described purpose is actually the functionality of the program. The purpose must address the problem being solved or creative interest being pursued through the program. The function is the behavior of a program during execution and is often described by how a user interacts with it. 

## Row 2 - Data Abstraction
The Written Response:
- includes data stored in a list or other collection type
- date in list being actively used
- identifies the name of the variable being used
- describes what the data contained in this list is representing in the program.
- must include two clearly distinguishable program code segments, but these
segments may be disjointed code segments or two parts of a contiguous code segment

You will not get a point if: 
- The list is a one-element list
- The use of the list does not assist in fulfilling the program’s purpose

## Row 3 - Managing Complexity
The Written Response:
- use a list to improve organization
- explain the alternative to using this list and why or why you wouldn't do that

You will not get a point if:
- the written response does not name the list
- the use of the list does not result in a program that is easier to develop
- tThe solution without the list is implausible, inaccurate, or inconsistent with the program

## Row 4 - Procedural Abstraction
The Written Response:
- a student-developed procedure with at least one parameter that has an effect on the functionality of the procedure
- a code segment where the student-developed procedure is being called
- describes what the procedure does and how it contributes to the code

You will not get a point if:
- the code segment is not specified
- the procedure is pre-made
- the procedure does not tie into the full code

## Row 5 - Algorithm Implementation
The Written Response:
- the program includes sequencing, selection, and iteration
- explains the algorithim well enough that someone else could recreate it

You will not get a point if:
- you do not explain **why**
- algorithm does not match the code
- algorithm is not identified
- the outcome does not effect the program

## Row 6 - Testing
The Written Response:
- has two calls to the selected procedure identified in written response 3c
    - each call must have a different argument
- describes the condition(s) being tested by each call to the procedure.
- identifies the result of each call

You will not get a point if:
- A procedure is not identified in written response 3c
- The written response for 3d does not apply to the procedure in 3c
- the calls are for the exact same code even if the result if different
- the response reflects conditions being tested that are implausible, inaccurate, or inconsistent with the program
