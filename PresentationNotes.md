# Presentation Notes
## Group C
- understand the requirments and weights of the 5 Big Ideas
- use Test Guide!
    - practice test
    - full breakdown of the test as well as stats of past tests
- utilize stratagies like repetition while studying

## [Practice Questions](https://www.test-guide.com/quiz/ap-comp-sci-4.html)
1. A new studio was set up in Ohio for a popular game development company. The company used 32-bit computers to develop most of their older games, but they require the new studio only to operate 64-bit machines. How many more bits will a single new machine offer compared to the PCs at the old studio?
    - 32 bits more than previously employed systems.
    - 2^32 bits more than previously employed systems
    - **2^64 bits more than previously employed systems.**
    - Twice as many bits as the previously employed systems.
2. A software development company breaks down its software projects into small modules so that coders can develop chunks of the code separately and then to the project once completed. What development process is the company following?
    - Additive
    - **Incremental**
    - Iterative
    - Spiral
3. A programmer developed the following algorithm to simulate coin flipping. The program simulates a coin flip ten times and compares the number of times the result was a head or tail. Which of the following shows that the coin flipping resulted in heads and tails equally?
    1.  Variables, ﬂips, and tails are initialized to 0. 
    2. Another variable, the coin, is assigned a random value between zero (tails) and one (heads). For one value, the tails variable is incremented by one. The ﬂip variable is incremented by 1 regardless of coin value.
    3. The second step is repeated until the ﬂip variable has a value of 10.
    - variable coin = 1
    - variable ﬂip = 1
    - variable ﬂip = 2
    - **variable tails = 5**

